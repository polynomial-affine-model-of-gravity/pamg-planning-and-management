#+title: Weekly meeting: <2024-07-26 Fri>
#+author: Oscar Castillo-Felisola 
#+options: toc:nil
#+where: ZOOM
#+project: PAMG weekly meeting

** Participants

- [X] Oscar Castillo-Felisola
- [ ] Bastian Grez           
- [X] Manuel Morocho         
- [X] Aureliano Skirzewski   
- [X] Jefferson Vaca         
- [X] Jose Perdiguero        
- [ ] Nicolas Zambra         
- [ ] Oscar Orellana         
- [ ] Alfonso Zerwekh        

* ACTIONS

#+BEGIN: columnview :id global :match "/TODO|DONE" :format "%ITEM(What) %TAGS(Who) %DEADLINE(When) %TODO(State)"
| What                                                              | Who      | When             | State |
|-------------------------------------------------------------------+----------+------------------+-------|
#+END:

* DECISIONS

#+BEGIN: columnview :id global :match "Decision|News" :format "%ITEM(Decisions)"
| Decisions                                             |
|-------------------------------------------------------|
#+END:

* Notes

,- Use =:Decision:= tag for decision
,- Use entry with =TODO= (or =DONE=) for actions

** Discussion of Harada's model                                         :OCF:

Harada proposed a model (aka Conformal Killing Gravity [[cite:&harada21_emerg_cotton_tensor_descr_gravit;&harada23_dark_energ_confor_killin_gravit]])

** Discussion of the advances by Nicolas                                :OCF:

Nicolas and I discussed during the week, possibles strategies to pursue when considering the spherical ansatz for the affine connection.

The simplest connections should behave as Minkowskian connections. Using the form of the Minkowskian connection to fixing some of the components of the spherical affine connection, there are three solutions to the field equations:
- Minkowski,
- AdS,
- A novel solution. 

*** TODO Does the new solution corresponds to the one of Harada?            :NZ:

Harada reports a new Schwarzschild-like solution in Ref. [[cite:&harada21_emerg_cotton_tensor_descr_gravit]].

** Practice of Manuel thesis                                             :MM:


