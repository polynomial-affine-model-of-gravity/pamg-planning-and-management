#+title: Weekly meeting: <2024-03-08 Fri>
#+author: Oscar Castillo-Felisola 
#+options: toc:nil
#+where: ZOOM
#+when: <2024-03-08 Fri>
# #+absent: C. Robert,T. tartanpion
#+present: Oscar Castillo-Felisola 
# #+excuse:Sophie Fonsec,Karine Soulet
#+initiator: Oscar Castillo-Felisola
#+project: PAMG weekly meeting
#+duration: 1.5H
# #+logo: logo.png

** Participants

| Present                 | Absent         |
|-------------------------+----------------|
| Oscar Castillo-Felisola | Oscar Orellana |
| Aureliano Skirzewski    |                |
| Jose Perdiguero         |                |
| Bastian Grez            |                |
| Manuel Morocho          |                |
| Nicolas Zambra          |                |
| Alfonso Zerwekh         |                |
| Jefferson Vaca          |                |

* ACTIONS

#+BEGIN: columnview :id global :match "/TODO|DONE" :format "%ITEM(What) %TAGS(Who) %DEADLINE(When) %TODO(State)"
| What                                             | Who        | When | State |
|--------------------------------------------------+------------+------+-------|
| Resend to Orellana the PDE of the spherical case | :NZ:JP:OO: |      | DONE  |
| Classification of three-dimensional solutions    | :BG:OCF:   |      | DONE  |
| Start the repository for other projects          | :OCF:      |      | DONE  |
#+END:

* DECISIONS

#+BEGIN: columnview :id global :match "Decision" :format "%ITEM(Decisions)"
| Decisions                                               |
|---------------------------------------------------------|
| Hold the fluctuation of torsion until inflation is sent |
| The inflation paper would be send to JCAP               |
#+END:

* Notes

,- Use =:Decision:= tag for decision
,- Use entry with =TODO= (or =DONE=) for actions

** Hold the fluctuation of torsion until inflation is sent         :Decision:

** DONE Resend to Orellana the PDE of the spherical case              :NZ:JP:OO:

Jose proposed a method to solve the equations for the harmonic curvature case, using analogies with the harmonic oscillator.

** DONE Classification of three-dimensional solutions                   :BG:OCF:

Bastian has been working in the classification of the cosmological solutions to the three-dimensional model.

We already have a GitLab repository.

** The inflation paper would be send to JCAP                       :Decision:

** DONE Start the repository for other projects                            :OCF:
